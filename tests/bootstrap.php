<?php
/**
 * Requires the class Template
 */
date_default_timezone_set('Europe/Amsterdam');

$ds = DIRECTORY_SEPARATOR;
require_once dirname(dirname(__FILE__)) . $ds . 'lib' . $ds . 'Template' . $ds . 'Template.php';
