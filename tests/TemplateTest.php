<?php


class TemplateTest extends PHPUnit_Framework_TestCase {

	public function testRender() {
		$templatesDir = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
			$tpl = new Template();
		$tpl->addTemplatePath($templatesDir);
		$result = $tpl->render('template-1');
		$this->assertStringEqualsFile($templatesDir . 'template-1.tpl.php', $result);
	}

}
