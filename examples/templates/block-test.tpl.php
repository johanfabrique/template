<?php
/**
 * @var Template\TemplateContext $this
 */

$this->extend('block-test-parent');
?>
<?php $this->start('block'); ?>
Start from inner template!!
Super (<?php echo $this->super(); ?>)
End inner template!!
<?php $this->end(); ?>

<h2>HTML helper</h2>
<p>URL to /test: <?php echo $this->Html->url('/test'); ?></p>

<p>Asset URLs:</p>
<dl>
	<dt>CSS</dt>
	<dd><?php echo $this->Html->css('theme1/screen.css'); ?></dd>
	<dt>JS</dt>
	<dd><?php echo $this->Html->js('lib/jquery.js'); ?></dd>
	<dt>Image</dt>
	<dd><?php echo $this->Html->img('/icons/pdf.png'); ?></dd>
</dl>
