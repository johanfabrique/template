<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
if (function_exists('opcache_reset')) {
	opcache_reset();
}

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)) . DS);

require_once ROOT . 'lib/Template/Template.php';

$tpl = new \Template\Template(array(
	'path' => array(ROOT . 'examples/templates'),
	'helpers' => array(
		'Html'
	)
));

echo $tpl->render('block-test');
