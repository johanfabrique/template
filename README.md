# Template engine

## Short cuts

Install Composer: `$ curl -sS https://getcomposer.org/installer | php -- --install-dir=bin`

Generate documentation: `$ php vendor/bin/phpdoc.php`

Run unit tests: `$ phpunit --bootstrap tests/bootstrap.php tests`

## (Auto)loading

Add the following to your `composer.json`:
```
#!json
{
	"repositories": [
		{
			"type": "vcs",
			"url": "git@bitbucket.org:frontendr/template.git"
		}
	],
	"require": {
		"frontendr/template": "master"
	}
}

```
Then run `$ php bin/composer.phar update` to update the dependencies.
And add the autoloader to your project:
```
#!php
<?php
require 'vendor/autoload.php';
```