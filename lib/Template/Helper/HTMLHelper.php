<?php
/**
 * Template class
 *
 * @author Johan Arensman <johan@frontendr.com>
 */

namespace Template;

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'TemplateHelper.php';

/**
 * HTML Helper class
 * @package Template
 */
class HtmlHelper extends TemplateHelper {

	/**
	 * @var string Base URL used for URL functions
	 */
	private $baseURL;

	/**
	 * @var array Default settings for the HTML helper
	 */
	public $__settings = array(
		'paths' => array(
			'img' => 'img/',
			'css' => 'css/',
			'js' => 'js/'
		)
	);

	/**
	 * @param array $settings
	 */
	public function __construct(array $settings) {
		parent::__construct($settings);

		// Set the base path:
		$parts = explode('/', $_SERVER['SCRIPT_FILENAME']);
		$indexFile = array_pop($parts);
		$this->baseURL = substr($_SERVER['SCRIPT_NAME'], 0, -strlen($indexFile));
	}

	/**
	 * Returns the absolute url to the given path
	 * @param string $path The path to create the URL for
	 * @return string
	 */
	public function url($path) {
		return $this->assetUrl(__FUNCTION__, $path);
	}

	/**
	 * Returns the absolute path to the given image in the image folder
	 * @param string $path
	 * @return string
	 */
	public function img($path) {
		return $this->assetUrl(__FUNCTION__, $path);
	}

	/**
	 * Returns the absolute path to the given css file in the css folder
	 * @param string $path
	 * @return string
	 */
	public function css($path) {
		return $this->assetUrl(__FUNCTION__, $path);
	}

	/**
	 * Returns the absolute path to the given javascript file in the javascript folder
	 * @param string $path
	 * @return string
	 */
	public function js($path) {
		return $this->assetUrl(__FUNCTION__, $path);
	}

	/**
	 * Returns the absolute path to the given asset type
	 * @param string $type Type of asset
	 * @param string $path
	 * @return string
	 */
	private function assetUrl($type, $path) {
		$base = $this->baseURL;
		if (array_key_exists($type, $this->__settings['paths'])) {
			$base .= $this->__settings['paths'][$type];
		}
		return $base . ltrim($path, '/');
	}
}
