<?php
/**
 * Template class
 *
 * @author Johan Arensman <johan@frontendr.com>
 */

namespace Template;

/**
 * A template block serves as an output container for a block to which can be prepended or appended.
 *
 * @package Template
 * @version 1.0
 */
class TemplateBlock {

	/**
	 * @var string The content of this block.
	 */
	private $content = '';

	/**
	 * @var array Super parts of this block.
	 */
	private $super = null;

	/**
	 * Creates a new block with the provided content.
	 * @param string $content
	 */
	public function __construct($content = '') {
		$this->content = $content;
	}

	/**
	 * Sets the new value of this block or ends the block with a super() call.
	 * @param string $content
	 * @return TemplateBlock
	 */
	public function assign($content) {
		if (!empty($this->super)) {
			$this->super[] = $content;
		} else {
			$this->content = $content;
		}
		return $this;
	}

	/**
	 * Prepends the given value to this block and optionally ends this block.
	 * @param string $content
	 * @return TemplateBlock
	 */
	public function prepend($content) {
		if (!empty($this->super)) {
			$this->super[] = $content;
		} else {
			$this->content = $content . $this->content;
		}
		return $this;
	}

	/**
	 * Appends the given value to this block and optionally ends this block.
	 * @param string $content
	 * @return TemplateBlock
	 */
	public function append($content) {
		if (!empty($this->super)) {
			$this->super[] = $content;
		} else {
			$this->content = $this->content . $content;
		}
		return $this;
	}

	/**
	 * Sets the given content as the top super content.
	 * @param string $content
	 * @return string Intentionally a blank string to be able to 'print' this method.
	 */
	public function super($content) {
		$this->super = array($content);
		return '';
	}

	/**
	 * Returns the string content of this block,
	 * @return string
	 */
	public function __toString() {
		$content = $this->content;

		if (!empty($this->super)) {
			return $this->super[0] . $content . $this->super[1];
		}
		return $this->content;
	}
}
