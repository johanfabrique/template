<?php
/**
 * Template class
 *
 * @author Johan Arensman <johan@frontendr.com>
 */

namespace Template;

require_once 'TemplateContext.php';

/**
 * Template class
 * Primary object which handles searching for template and acts as the primary interface to the templates
 * on the side of the controller.
 *
 * @package Template
 * @version 1.0
 */
class Template {

	/**
	 * @var array Settings array provided to the constructor
	 */
	private $settings = array();

	/**
	 * @var string The extension which is appended to template names.
	 * @see Template\Template::setTemplateExtension()
	 */
	private $templateExtension = '.tpl.php';

	/**
	 * @var string The class name of the template context to use.
	 */
	private $templateContextClass = 'Template\TemplateContext';

	/**
	 * @var string The default class to use as a template context.
	 */
	private $defaultTemplateContextClass = 'Template\TemplateContext';

	/**
	 * @var string The absolute class path of this class with trailing directory separator
	 */
	private $classPath;

	/**
	 * @var array The paths in which templates are searched.
	 */
	private $templatePaths = array();

	/**
	 * @var array Caching array which maps template names to file system paths.
	 */
	private $resolvedTemplatePaths = array();

	/**
	 * @var array Associative array of variables to assign to the template.
	 */
	private $variables = array();

	/**
	 * @var array Array of available helper classes.
	 */
	private $helpers = array();

	/**
	 * @var array Associative array of functions available in the template context.
	 */
	private $functions = array();


	/**
	 * Creates a new instance of the Template class and makes sure the TemplateContext class is loaded.
	 *
	 * @param array [$settings] Optional associative array with settings.
	 * Each key is a call to one of the existing setting methods:
	 * 'path' or 'paths' with a string or array of paths are mapped to addTemplatePath().
	 * 'extension' calls setTemplateExtension() with the given value.
	 * 'set' or 'variables' with an array of key value pairs are mapped to set().
	 */
	public function __construct(array $settings = null) {
		$this->classPath = __DIR__ . DIRECTORY_SEPARATOR;

		if (!empty($settings)) {
			$this->settings = $settings;
			foreach ($settings as $key => $value) {
				$method = null;
				switch ($key) {
					case 'context':
						$this->setTemplateContext($value);
						break;
					case 'path':
					case 'paths':
						$method = 'addTemplatePath';
						break;
					case 'extension':
						$method = 'setTemplateExtension';
						break;
					case 'set':
					case 'variables':
						$method = 'set';
						break;
					case 'functions':
						$this->registerContextFunction($value);
						break;
					case 'helpers':
						$this->registerHelper($value);
				}

				if ($method) {
					array_map(array($this, $method), (array)$value);
				}
			}
		}

		if (is_string($this->templateContextClass) && !class_exists($this->templateContextClass)) {
			$this->templateContextClass = $this->defaultTemplateContextClass;
			/** @noinspection PhpIncludeInspection */
			require_once sprintf('%s.php', $this->defaultTemplateContextClass);
		}
	}

	/**
	 * Sets the given context as the template context class to use
	 * @param string|object $class Either the name of the class or the class object itself.
	 * @return $this
	 */
	public function setTemplateContext($class) {
		if ((is_string($class) && class_exists($class)) || is_object($class)) {
			$this->templateContextClass = $class;
		}

		return $this;
	}

	/**
	 * Adds the given path to the list of paths in which templates are searched for.
	 * @param string $path The path to the folder which contains templates.
	 * @return Template
	 */
	public function addTemplatePath($path) {
		$path = realpath($path) . DIRECTORY_SEPARATOR;
		if (!in_array($path, $this->templatePaths)) {
			array_push($this->templatePaths, $path);
		}

		return $this;
	}

	/**
	 * Sets the extension which is appended to template names when searching for templates.
	 * @param string $extension The extension such as '.tpl.php'.
	 * @return Template
	 */
	public function setTemplateExtension($extension) {
		$this->templateExtension = $extension;
		return $this;
	}

	/**
	 * Searches all template paths for the given template name and returns the absolute path to the template file.
	 * @param string $name The name of the template to search for.
	 * @return bool|string Returns the absolute path to the template or false when the template is not found.
	 */
	public function getTemplatePath($name) {
		if (array_key_exists($name, $this->resolvedTemplatePaths)) {
			return $this->resolvedTemplatePaths[$name];
		}
		foreach ($this->templatePaths as $path) {
			$templatePath = $path . $name . $this->templateExtension;
			if (file_exists($templatePath)) {
				return $this->resolvedTemplatePaths[$name] = $templatePath;
			}
		}

		return false;
	}

	/**
	 * Returns whether the given template exists
	 * @param string $name The name of the template.
	 * @return bool True when the template exists.
	 */
	public function templateExists($name) {
		return !!$this->getTemplatePath($name);
	}

	/**
	 * Set a variable or array of variables which becomes available in the template.
	 * @param string|array $var Either a string with the name of the variable or an associative array of which
	 * the keys are used as variable names and the values as variable values.
	 * @param mixed $value The value of the given variable name.
	 * @return Template
	 */
	public function set($var, $value = null) {
		if (is_array($var)) {
			foreach ($var as $key => $value) {
				$this->set($key, $value);
			}
		} else {
			$this->variables[$var] = $value;
		}

		return $this;
	}

	/**
	 * Registers a function which will become available in the template context.
	 * @param string|array $name The name of the function or an associative array of function names and callbacks.
	 * @param callable $callback The callback to register.
	 * @return Template
	 */
	public function registerContextFunction($name, $callback = null) {
		if (is_array($name)) {
			foreach ($name as $functionName => $callback) {
				$this->registerContextFunction($functionName, $callback);
			}
		} elseif (is_callable($callback)) {
			$this->functions[$name] = $callback;
		}

		return $this;
	}

	/**
	 * Registers TemplateHelper instances by name.
	 * @param string|array $name Name or array of helper names to register
	 * @return Template
	 */
	public function registerHelper($name) {
		if (is_array($name)) {
			foreach ($name as $helper) {
				$this->registerHelper($helper);
			}
		} else {
			$className = ucfirst($name) . 'Helper';
			$nameSpacedClassName = 'Template\\' . $className;

			if (!class_exists($nameSpacedClassName)) {
				/** @noinspection PhpIncludeInspection */
				require_once __DIR__ . DIRECTORY_SEPARATOR . 'Helper' . DIRECTORY_SEPARATOR . $className . '.php';
			}

			if (!class_exists($nameSpacedClassName)) {
				trigger_error(
					sprintf("Unable to load helper class '%s' (%s)!", $className, $nameSpacedClassName),
					E_USER_ERROR
				);
			}
			$params = array_key_exists($name, $this->settings) ? $this->settings[$name] : array();
			$this->helpers[$name] = new $nameSpacedClassName($params);
		}

		return $this;
	}

	/**
	 * Renders the given template name.
	 * @param string $name The template name.
	 * @return string The rendered template.
	 */
	public function render($name) {
		/** @var TemplateContext $context */
		$context = new $this->templateContextClass($this, $this->variables, $this->functions, $this->helpers);

		return $context->render($name);
	}
}
