<?php
/**
 * Template class
 *
 * @author Johan Arensman <johan@frontendr.com>
 */

namespace Template;

require_once 'TemplateBlock.php';

/**
 * Object which serves as a context for each template
 *
 * @property mixed Html
 * @package Template
 * @version 1.0
 */
class TemplateContext {

	/**
	 * @var array The variables in this context which are available in the context.
	 */
	private $variables = array();

	/**
	 * @var array Additional in this context which are available in the context.
	 */
	private $functions = array();

	/**
	 * @var array Associative array of registered helpers.
	 */
	private $helpers = array();

	/**
	 * @var TemplateBlock[] Captured blocks.
	 * @see beginBlock()
	 */
	private $blocks = array();

	/**
	 * @var array The current stack of content blocks, the last one being the current.
	 */
	private $blockStack = array();

	/**
	 * @var string|null The path to the template which extends the current template.
	 * @see extend()
	 */
	private $extends = null;

	/**
	 * @var Template The Template handler instance which is used to retrieve template paths.
	 */
	private $Template;

	/**
	 * @var string|null Content container for all captured content for extending views.
	 */
	public $content = null;

	/**
	 * Constructs a new TemplateContext with the provided variables.
	 *
	 * @param Template $template
	 * @param array $variables Optional array with variables available in this context.
	 * @param array $functions Optional array with function names and callable functions.
	 * @param array $helpers Optional array with helper names and their instances
	 */
	public function __construct(Template $template, array $variables = array(), array $functions = array(), array $helpers = array()) {
		$this->Template = $template;
		$this->variables = $variables;
		if (!empty($functions)) {
			$this->functions = array_diff_key($functions, array_flip(get_class_methods($this)));
		}
		if (!empty($helpers)) {
			$this->helpers = $helpers;
			foreach ($helpers as $name => $instance) {
				$this->{$name} = $instance;
			}
		}
	}

	/**
	 * toString method
	 * @return string
	 */
	public function __toString() {
		return '';
	}

	/**
	 * Magic call method
	 * @param string $name The name of the function to call
	 * @param array $arguments Array of arguments passed to the function
	 * @return mixed|string
	 */
	public function __call($name, $arguments) {
		if (array_key_exists($name, $this->functions)) {
			return call_user_func_array($this->functions[$name], $arguments);
		}
		return sprintf("[Template error: Method '%s' doesn't exist!]", $name);
	}

	/**
	 * Renders the specified template and handles extending it.
	 * @param string $name The name of the template to render.
	 * @param array $variables Optional associative array of variables to pass to this template.
	 * @see renderTemplate()
	 * @return string The rendered template.
	 */
	public function render($name, $variables = array()) {
		$templatePath = $this->Template->getTemplatePath($name);
		if (!$templatePath) {
			trigger_error(
				sprintf("No path was found for the template '%s'!", $name),
				E_USER_WARNING
			);
			return false;
		}

		$templateVariables = $this->variables;
		if (!empty($variables) && is_array($variables)) {
			$templateVariables = array_merge($templateVariables, $variables);
		}

		$templateContent = null;
		do {
			$this->content = $templateContent;
			$templateContent = $this->renderTemplate($templatePath, $templateVariables);
			$templatePath = $this->extends;
			$this->extends = null;
		} while (!is_null($templatePath));

		return $templateContent;
	}

	/**
	 * Renders the specified template file and returns the result
	 * @param string $__template_path The path of the template file
	 * @param array $__template_variables An associative array of variables which will
	 * be extracted in the templates scope.
	 * @return string The templates output.
	 */
	private function renderTemplate($__template_path, $__template_variables) {
		extract($__template_variables);
		ob_start();
		/** @noinspection PhpIncludeInspection */
		include $__template_path;
		return ob_get_clean();
	}

	/**
	 * Renders an external template which receives all of the variables of this context and returns the result.
	 * @param string $name The name of the template to render.
	 * @param array $variables Optional array of variables which will be available in the template.
	 * @return string The rendered template.
	 */
	public function element($name, $variables = array()) {
		$contextVariables = $this->variables;
		if (!empty($variables) && is_array($variables)) {
			$contextVariables = array_merge($contextVariables, $variables);
		}
		$context = new TemplateContext($this->Template, $contextVariables, $this->functions, $this->helpers);

		return $context->render($name);
	}

	/**
	 * Returns the content of the block.
	 * @param string $name The name of the block.
	 * @param string $default The default value of this block when it doesn't exist.
	 * @return string|null The contents of the block or the value of $default when the block doesn't exist.
	 */
	public function fetch($name, $default = '') {
		$block = $default;
		if (array_key_exists($name, $this->blocks)) {
			$block = $this->blocks[$name];
		}

		return $block;
	}

	/**
	 * Assigns a value to a block, overwriting it if it already exists.
	 * @param string $name The name of the block.
	 * @param mixed $content The contents of the block.
	 * @param string $mode The method to use when storing the content of the block.
	 * @return TemplateContext
	 */
	public function assign($name, $content, $mode = 'assign') {
		if (array_key_exists($name, $this->blocks)) {
			$block = $this->blocks[$name];
			if (!is_callable(array($block, $mode))) {
				$mode = 'assign';
			}
			$block->$mode($content);
		} else {
			$this->blocks[$name] = new TemplateBlock($content);
		}

		return $mode === 'block' ? $this->fetch($name) : $this;
	}

	/**
	 * Starts capturing a new block. If the block exists it will be overwritten.
	 * @param string $name The name of the block to start.
	 * @return TemplateContext
	 */
	public function start($name) {
		return $this->beginBlock(__FUNCTION__, $name);
	}

	/**
	 * Starts capturing content to be appended to an existing block.
	 * @param string $name The name of the block to append to.
	 * @return TemplateContext
	 */
	public function append($name) {
		return $this->beginBlock(__FUNCTION__, $name);
	}

	/**
	 * Starts capturing content to be prepended to an existing block.
	 * @param string $name The name of the block to prepend to.
	 * @return TemplateContext
	 */
	public function prepend($name) {
		return $this->beginBlock(__FUNCTION__, $name);
	}

	/**
	 * Starts capturing content for a block that is immediately printed when ended.
	 * @param string $name The name of the block to print
	 * @return TemplateContext
	 */
	public function block($name) {
		return $this->beginBlock(__FUNCTION__, $name);
	}

	/**
	 * Starts capturing content to be saved as a block.
	 * @param string $mode The mode to use when ending the block. Either 'start', 'append' or 'prepend'.
	 * @param string $block The name of the block.
	 * @see start()
	 * @see append()
	 * @see prepend()
	 * @return TemplateContext
	 */
	private function beginBlock($mode, $block) {
		$this->blockStack[] = array($mode, $block);

		if (!array_key_exists($block, $this->blocks)) {
			$this->blocks[$block] = new TemplateBlock();
		}

		ob_start();

		return $this;
	}

	/**
	 * Sets the position on which the content of the block which is extended.
	 */
	public function super() {
		end($this->blockStack);
		list(, $block) = current($this->blockStack);
		$this->blocks[$block]->super(ob_get_clean());

		return '';
	}

	/**
	 * Ends the currently started or appending block using the mode specified by the method that started the block.
	 * @see beginBlock()
	 * @return TemplateContext
	 */
	public function end() {
		if (empty($this->blockStack)) {
			trigger_error("Unable to end a block because no block has been started.", E_USER_WARNING);
			return $this;
		}

		list($mode, $block) = array_pop($this->blockStack);

		$content = ob_get_clean();

		echo $this->assign($block, $content, $mode);

		return $this;
	}

	/**
	 * Extend the current template as 'content' for the given template.
	 * @param string $name The name of the template for which this template is going to serve as 'content'.
	 * @return TemplateContext
	 */
	public function extend($name) {
		$path = $this->Template->getTemplatePath($name);
		if (!$path) {
			trigger_error(
				sprintf("Unable to extend the template because template '%s' doesn't exist.", $name),
				E_USER_ERROR
			);
		} else {
			$this->extends = $path;
		}

		return $this;
	}
}
