<?php
/**
 * Template class
 *
 * @author Johan Arensman <johan@frontendr.com>
 */

namespace Template;

/**
 * Class available in the template context to provide helper functions.
 *
 * @package Template
 * @version 1.0
 */
class TemplateHelper {

	/**
	 * @var array $settings
	 */
	public $__settings = array();

	/**
	 * @param array $settings
	 */
	public function __construct(array $settings) {
		$this->__settings = array_merge_recursive($this->__settings, $settings);
	}

}
